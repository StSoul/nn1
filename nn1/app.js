'use strict';

const getRandom = (min = -1, max = 1) => Math.random() * (max - min) + min;
const sigmoid = (x) => 1 / (1 + Math.pow(Math.exp(1), -x));
const MSE = (expected, output) => (Math.pow(expected - output, 2) * 100).toFixed(2) + '%';

let l1_h1 = getRandom(), l1_h2 = getRandom(),
    l2_h1 = getRandom(), l2_h2 = getRandom(),
    l3_h1 = getRandom(), l3_h2 = getRandom(),

    h1_o = getRandom(), h2_o = getRandom();

function predict(data) {
    const inputs = data[0];

    const h_1_input = inputs[0] * l1_h1 + inputs[1] * l2_h1 + inputs[2] * l3_h1;
    const h_2_input = inputs[0] * l1_h2 + inputs[1] * l2_h2 + inputs[2] * l3_h2;

    const h_1_output = sigmoid(h_1_input);
    const h_2_output = sigmoid(h_2_input);

    const o_input = h_1_output * h1_o + h_2_output * h2_o;

    return sigmoid(o_input);
}

function train(data) {

    const inputs = data[0], expectedResult = data[1];

    const h_1_input = inputs[0] * l1_h1 + inputs[1] * l2_h1 + inputs[2] * l3_h1;
    const h_2_input = inputs[0] * l1_h2 + inputs[1] * l2_h2 + inputs[2] * l3_h2;

    const h_1_output = sigmoid(h_1_input);
    const h_2_output = sigmoid(h_2_input);

    const o_input = h_1_output * h1_o + h_2_output * h2_o;
    const o_output = sigmoid(o_input);

    // Ошибка на выходном нейроне
    //
    const error = expectedResult - o_output;
    const sigmoidDerivative = o_output * (1 - o_output);
    const weightsDelta = error * sigmoidDerivative;

    // Изменяем веса выходного нейрона
    //
    h1_o += h_1_output * weightsDelta * learningRate;
    h2_o += h_2_output * weightsDelta * learningRate;

    // Ошибка на первом скрытом нейроне
    //
    const error_h1 = h1_o * weightsDelta;
    const sigmoidDerivative_h1 = h_1_output * (1 - h_1_output);
    const weightsDelta_h1 = error_h1 * sigmoidDerivative_h1;

    // Изменяем веса для первого скрытого нейрона
    //
    l1_h1 += inputs[0] * weightsDelta_h1 * learningRate;
    l2_h1 += inputs[1] * weightsDelta_h1 * learningRate;
    l3_h1 += inputs[2] * weightsDelta_h1 * learningRate;


    // Ошибка на втором скрытом нейроне
    //
    const error_h2 = h2_o * weightsDelta;
    const sigmoidDerivative_h2 = h_2_output * (1 - h_2_output);
    const weightsDelta_h2 = error_h2 * sigmoidDerivative_h2;

    // Изменяем веса второго скрытого нейрона
    //
    l1_h2 += inputs[0] * weightsDelta_h2 * learningRate;
    l2_h2 += inputs[1] * weightsDelta_h2 * learningRate;
    l3_h2 += inputs[2] * weightsDelta_h2 * learningRate;

    return {result: o_output, error, expected: expectedResult}
}

const epochs = 1000;
const learningRate = 1.5;

const trainingSet2 = [
    [[0, 0, 0], 0],
    [[0, 0, 1], 1],
    [[0, 1, 0], 0],
    [[0, 1, 1], 0],
    [[1, 0, 0], 1],
    [[1, 0, 1], 1],
    [[1, 1, 0], 0],
    [[1, 1, 1], 1]
];

const trainingSet = [
    [[0, 0, 0, 0], 0],
    [[0, 0, 1, 1], 1],
    [[0, 1, 0, 1], 0],
    [[0, 1, 1, 1], 0],
    [[1, 1, 0, 0], 0],
    [[1, 0, 0, 1], 1],
    [[1, 0, 0, 1], 1],
    [[1, 1, 1, 1], 1]
];

function training() {
    for (let i = 0; i < epochs; i++) {
        for (let a = 0; a < trainingSet.length; a++) {
            train(trainingSet[a]);
        }
    }
}

training();

console.log(`=================  Результат ===================`);
for (let a = 0; a < trainingSet.length; a++) {
    const expected = trainingSet[a][1];
    const output = predict(trainingSet[a]);
    const difference = Math.abs(expected - output);
    const success = difference < 0.25;

    console.log(`Ожидалось/получено: ${trainingSet[a][1]}/${output} (${success}); Error(MSE) = ${MSE(expected, output)}`);
}

console.log(`======================`);
console.log(predict([[1, 1, 0, 0]]));

// console.log(`\n=================  Веса ===================`);
// console.log(l1_h1, l2_h1, l3_h1, l1_h2, l2_h2, l3_h2);
// console.log(`---------------------`);
// console.log(h1_o, h2_o);

