'use strict';

const getRandom = (min = -1, max = 1) => Math.random() * (max - min) + min;
const sigmoid = (x) => 1 / (1 + Math.pow(Math.exp(1), -x));
const MSE = (expected, output) => (Math.pow(expected - output, 2) * 100).toFixed(1) + '%';

let l1_h1 = getRandom(), l1_h2 = getRandom(), l1_h3 = getRandom(),
    l2_h1 = getRandom(), l2_h2 = getRandom(), l2_h3 = getRandom(),
    l3_h1 = getRandom(), l3_h2 = getRandom(), l3_h3 = getRandom(),
    l4_h1 = getRandom(), l4_h2 = getRandom(), l4_h3 = getRandom(),

    h1_o1 = getRandom(), h2_o1 = getRandom(), h3_o1 = getRandom(),
    h1_o2 = getRandom(), h2_o2 = getRandom(), h3_o2 = getRandom();

let Delta_h1_o1 = 0, Delta_h2_o1 = 0, Delta_h3_o1 = 0, Delta_h1_o2 = 0, Delta_h2_o2 = 0, Delta_h3_o2 = 0,
    Delta_l1_h1 = 0, Delta_l2_h1 = 0, Delta_l3_h1 = 0, Delta_l4_h1 = 0,
    Delta_l1_h2 = 0, Delta_l2_h2 = 0, Delta_l3_h2 = 0, Delta_l4_h2 = 0,
    Delta_l1_h3 = 0, Delta_l2_h3 = 0, Delta_l3_h3 = 0, Delta_l4_h3 = 0;

function predict(inputs) {

    const h_1_input = inputs[0] * l1_h1 + inputs[1] * l2_h1 + inputs[2] * l3_h1 + inputs[3] * l4_h1;
    const h_2_input = inputs[0] * l1_h2 + inputs[1] * l2_h2 + inputs[2] * l3_h2 + inputs[3] * l4_h2;
    const h_3_input = inputs[0] * l1_h3 + inputs[1] * l2_h3 + inputs[2] * l3_h3 + inputs[3] * l4_h3;

    const h_1_output = sigmoid(h_1_input);
    const h_2_output = sigmoid(h_2_input);
    const h_3_output = sigmoid(h_3_input);

    const o1_input = h_1_output * h1_o1 + h_2_output * h2_o1 + h_3_output * h3_o1;
    const o2_input = h_1_output * h1_o2 + h_2_output * h2_o2 + h_3_output * h3_o2;

    return {
        o1_output:  sigmoid(o1_input), o2_output:  sigmoid(o2_input),
        h_1_output, h_2_output, h_3_output
    };
}

function train(inputs, expected) {
    const {o1_output, o2_output, h_1_output, h_2_output, h_3_output} = predict(inputs);

    // Ошибка на первом выходном нейроне
    //
    const error1 = expected[0] - o1_output;
    const sigmoidDerivative1 = o1_output * (1 - o1_output);
    const weightsDelta1 = error1 * sigmoidDerivative1;

    // Ошибка на втором выходном нейроне
    //
    const error2 = expected[1] - o2_output;
    const sigmoidDerivative2 = o2_output * (1 - o2_output);
    const weightsDelta2 = error2 * sigmoidDerivative2;

    // Велчина на которую нужно изменить веса первого выходного нейрона
    //
    Delta_h1_o1 = h_1_output * weightsDelta1 * learningRate + Delta_h1_o1 * moment;
    Delta_h2_o1 = h_2_output * weightsDelta1 * learningRate + Delta_h2_o1 * moment;
    Delta_h3_o1 = h_3_output * weightsDelta1 * learningRate + Delta_h3_o1 * moment;

    // Велчина на которую нужно изменить веса второго выходного нейрона
    //
    Delta_h1_o2 = h_1_output * weightsDelta2 * learningRate + Delta_h1_o2 * moment;
    Delta_h2_o2 = h_2_output * weightsDelta2 * learningRate + Delta_h2_o2 * moment;
    Delta_h3_o2 = h_3_output * weightsDelta2 * learningRate + Delta_h3_o2 * moment;

    // Изменяем веса первого выходного нейрона
    //
    h1_o1 += Delta_h1_o1;
    h2_o1 += Delta_h2_o1;
    h3_o1 += Delta_h3_o1;

    // Изменяем веса второго выходного нейрона
    //
    h1_o2 += Delta_h1_o2;
    h2_o2 += Delta_h2_o2;
    h3_o2 += Delta_h3_o2;

    fix(weightsDelta1, h1_o1, h2_o1, h3_o1);
    fix(weightsDelta2, h1_o2, h2_o2, h3_o2);

    function fix(weightsDelta, h1_o, h2_o, h3_o) {
        // Ошибка на первом скрытом нейроне
        //
        const error_h1 = h1_o * weightsDelta;
        const sigmoidDerivative_h1 = h_1_output * (1 - h_1_output);
        const weightsDelta_h1 = error_h1 * sigmoidDerivative_h1;

        // Велчина на которую нужно изменить веса первого скрытого нейрона
        //
        Delta_l1_h1 = inputs[0] * weightsDelta_h1 * learningRate + Delta_l1_h1 * moment;
        Delta_l2_h1 = inputs[1] * weightsDelta_h1 * learningRate + Delta_l2_h1 * moment;
        Delta_l3_h1 = inputs[2] * weightsDelta_h1 * learningRate + Delta_l3_h1 * moment;
        Delta_l4_h1 = inputs[3] * weightsDelta_h1 * learningRate + Delta_l4_h1 * moment;

        // Изменяем веса для первого скрытого нейрона
        //
        l1_h1 += Delta_l1_h1;
        l2_h1 += Delta_l2_h1;
        l3_h1 += Delta_l3_h1;
        l4_h1 += Delta_l4_h1;

        // Ошибка на втором скрытом нейроне
        //
        const error_h2 = h2_o * weightsDelta;
        const sigmoidDerivative_h2 = h_2_output * (1 - h_2_output);
        const weightsDelta_h2 = error_h2 * sigmoidDerivative_h2;

        // Велчина на которую нужно изменить веса второго скрытого нейрона
        //
        Delta_l1_h2 = inputs[0] * weightsDelta_h2 * learningRate + Delta_l1_h2 * moment;
        Delta_l2_h2 = inputs[1] * weightsDelta_h2 * learningRate + Delta_l2_h2 * moment;
        Delta_l3_h2 = inputs[2] * weightsDelta_h2 * learningRate + Delta_l3_h2 * moment;
        Delta_l4_h2 = inputs[3] * weightsDelta_h2 * learningRate + Delta_l4_h2 * moment;

        // Изменяем веса второго скрытого нейрона
        //
        l1_h2 += Delta_l1_h2;
        l2_h2 += Delta_l2_h2;
        l3_h2 += Delta_l3_h2;
        l4_h2 += Delta_l4_h2;

        // Ошибка на третьем скрытом нейроне
        //
        const error_h3 = h3_o * weightsDelta;
        const sigmoidDerivative_h3 = h_3_output * (1 - h_3_output);
        const weightsDelta_h3 = error_h3 * sigmoidDerivative_h3;

        // Велчина на которую нужно изменить веса второго скрытого нейрона
        //
        Delta_l1_h3 = inputs[0] * weightsDelta_h3 * learningRate + Delta_l1_h3 * moment;
        Delta_l2_h3 = inputs[1] * weightsDelta_h3 * learningRate + Delta_l2_h3 * moment;
        Delta_l3_h3 = inputs[2] * weightsDelta_h3 * learningRate + Delta_l3_h3 * moment;
        Delta_l4_h3 = inputs[3] * weightsDelta_h3 * learningRate + Delta_l4_h3 * moment;

        // Изменяем веса второго скрытого нейрона
        //
        l1_h2 += Delta_l1_h3;
        l2_h2 += Delta_l2_h3;
        l3_h2 += Delta_l3_h3;
        l4_h2 += Delta_l4_h3;
    }
}

const epochs = 100;
const learningRate = 1.1;
const moment = 0.9;

const trainingSet = [
    [[0, 0, 0, 0], [0, 1]],
    [[0, 1, 0, 1], [0, 1]],
    [[0, 1, 1, 1], [0, 1]],
    [[1, 1, 0, 0], [0, 1]],
    [[0, 0, 1, 1], [1, 0]],
    [[1, 0, 0, 1], [1, 0]],
    [[1, 0, 0, 1], [1, 0]],
    [[1, 1, 1, 1], [1, 0]]
];

function training() {
    for (let i = 0; i < epochs; i++) {
        for (let a = 0; a < trainingSet.length; a++) {
            train(trainingSet[a][0], trainingSet[a][1]);
        }
    }
}

training();

console.log(`=================  Результат ===================`);
for (let a = 0; a < trainingSet.length; a++) {
    const expected = trainingSet[a][1][0];
    const { o1_output, o2_output } = predict(trainingSet[a][0]);
    const difference = Math.abs(expected - o1_output);
    const success = difference < 0.25;

    console.log(`Ожидалось/получено: ${trainingSet[a][1]}/${[o1_output.toFixed(3), o2_output.toFixed(3)]} (${success}); Error(MSE) = ${MSE(expected, o1_output)}`);
}

console.log(`======================`);
console.log( predict([0, 0, 1, 1]) ); // [1, 0]

