'use strict';

const getRandom = (min = -1, max = 1) => Math.random() * (max - min) + min;
const sigmoid = (x) => 1 / (1 + Math.pow(Math.exp(1), -x));

function NN() {
    const inputCount = arguments[0];
    const outputCount = arguments[arguments.length - 1];
    const hiddenLayerCount = arguments.length - 2;

    this.weights = {};

    for (let i = 1; i <= inputCount; i++) {
        for (let h = 1; h <= arguments[1]; h++) {
            if (!this.weights.inputLayer) this.weights.inputLayer = {};
            this.weights.inputLayer[`l${i}_h1${h}`] = getRandom();
        }
    }

    if (hiddenLayerCount > 1) {
        for (let i = 1; i < hiddenLayerCount; i++) {
            for (let h = 1; h <= arguments[i]; h++) {
                for (let a = 1; a <= arguments[i+1]; a++) {
                    if (!this.weights.hiddenLayer) this.weights.hiddenLayer = {};
                    this.weights.hiddenLayer[`h${i}${h}_h${i+1}${a}`] = getRandom();
                }
            }
        }
    }

    for (let h = 1; h <= arguments[arguments.length - 2]; h++) {
        for (let o = 1; o <= outputCount; o++) {
            if (!this.weights.outputLayer) this.weights.outputLayer = {};
            this.weights.outputLayer[`h${hiddenLayerCount}${h}_o${o}`] = getRandom();
        }
    }

    this.predict = data => {
        const inputs = data[0];

        let arr = [];
        for (let o_output in this.outputLayer) {
            let o_output = sigmoid(  );
           //arr.push(o_output) =
        }

        const h_1_input = inputs[0] * l1_h1 + inputs[1] * l2_h1 + inputs[2] * l3_h1;
        const h_2_input = inputs[0] * l1_h2 + inputs[1] * l2_h2 + inputs[2] * l3_h2;

        const h_1_output = sigmoid(h_1_input);
        const h_2_output = sigmoid(h_2_input);

        const o_input = h_1_output * h1_o + h_2_output * h2_o;

        return sigmoid(o_input).toFixed(2);
    }

}

const NN1 = new NN(3, 3, 3, 10, 1);

console.log(NN1);