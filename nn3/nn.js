h = require('./helper');

function NN() {
    this.network = [];

    for (let i = 1; i < arguments.length; i++) {
        let layer = [];

        for (let a = 0; a < arguments[i]; a++) {
            layer.push({
                output: null,
                weights: h.addWeights(arguments[i - 1])
            });
        }
        this.network.push(layer);
    }

    this.predict = function (inputs) {
        for (let l = 0; l < this.network.length; l++) {

            for (let n = 0; n < this.network[l].length; n++) {
                let neural = this.network[l][n];
                let input = 0;

                for (let w = 0; w < neural.weights.length; w++) {
                    input += neural.weights[w] *
                        (this.network[l - 1] ?
                            this.network[l - 1][w].output :
                            inputs[w]);
                }
                neural.output = h.sigmoid(input);
            }
        }
        return this.network[this.network.length - 1].map(outputNeural => outputNeural.output.toFixed(10));
    };

    this.train = function (data, learningRate = 0.5) {
        const inputs = data[0], expected = data[1];
        const result = this.predict(inputs)[0];

        const error = expected - result;
        const sigmoidDerivative = result * (1 - result);
        const delta = error * sigmoidDerivative;

        for (let l = this.network.length - 1; l >= 0; l--) {
            const layer = this.network[l];
            const previousLayer = this.network[l - 1];
            const nextLayer = this.network[l + 1];

            if (l === this.network.length - 1) {
                const outputNeural = layer[0];

                for (let w = 0; w < outputNeural.weights.length; w++) {
                    outputNeural.weights[w] += previousLayer[w].output * delta * learningRate;
                }
            } else {

                for (let n = 0; n < layer.length; n++) {
                    let neural = layer[n];
                    let errorN = 0;

                    for (let nextN = 0; nextN < nextLayer.length; nextN++) {
                        errorN +=  nextLayer[nextN].weights[n] * delta;
                    }

                    const derivative = neural.output * (1 - neural.output);
                    const deltaN = errorN * derivative;

                    for (let w = 0; w < neural.weights.length; w++) {
                        neural.weights[w] += deltaN * learningRate * (previousLayer ?
                            previousLayer[w].output :
                            inputs[w]);
                    }
                }
            }
        }
    };

    return this;
}

const nn = new NN(30, 7, 7, 1);
//return console.log(nn.network[0]);

const trainingSet2 = [
    [[0, 0, 0], 0],
    [[0, 0, 1], 1],
    [[0, 1, 0], 0],
    [[0, 1, 1], 0],
    [[1, 0, 0], 1],
    [[1, 0, 1], 1],
    [[1, 1, 0], 0],
    [[1, 1, 1], 1],
];

const trainingSet1 = [
    [[0, 0, 0, 0], 0],
    [[0, 1, 0, 1], 1],
    [[1, 0, 0, 0], 1],
    [[0, 0, 1, 0], 1],
    [[0, 1, 0, 0], 0],
    [[0, 0, 1, 1], 1],
    [[0, 1, 1, 1], 1],
    [[1, 1, 0, 0], 0],
    [[1, 0, 0, 1], 1],
    [[1, 1, 1, 1], 1],
    [[1, 1, 0, 1], 0],
];

const trainingSet = [
    [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 1],
    [[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 0],
];

function training() {
    for (let i = 0; i < 1000; i++) {
        for (let a = 0; a < trainingSet.length; a++) {
            nn.train(trainingSet[a], 0.5);
        }
    }
}
training();

for (let a = 0; a < trainingSet.length; a++) {
    const expected = trainingSet[a][1];
    const output = nn.predict(trainingSet[a][0]);
    const difference = Math.abs(expected - output);
    const success = difference < 0.25;

    console.log(`Ожидалось/получено: ${trainingSet[a][1]}/${output} (${success}); Error(MSE) = ${h.MSE(expected, output)}`);
}
console.log(`====================================================`);
const output1 = nn.predict([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
const output2 = nn.predict([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
const output3 = nn.predict([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
const output4 = nn.predict([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
const output5 = nn.predict([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
const output6 = nn.predict([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
const output7 = nn.predict([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]);
const output8 = nn.predict([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
const output9 = nn.predict([1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1]);

console.log(`Получено: ${output1}`);
console.log(`Получено: ${output2}`);
console.log(`Получено: ${output3}`);
console.log(`Получено: ${output4}`);
console.log(`Получено: ${output5}`);
console.log(`Получено: ${output6}`);
console.log(`Получено: ${output7}`);
console.log(`Получено: ${output8}`);
console.log(`Получено: ${output9}`);
