module.exports = {
    getRandom: (min = -0.1, max = 0.1) => Math.random() * (max - min) + min,
    sigmoid: (x) => 1 / (1 + Math.pow(Math.exp(1), -x)),
    MSE: (expected, output) => (Math.pow(expected - output, 2) * 100).toFixed(2) + '%',
    addWeights: function (length)  {
        let arr = [];
        for(let i = 0; i < length; i++) {
            arr.push(this.getRandom());
            //arr.push(testWeights.shift());
        }
        return arr;
    }
};

let testWeights = [
    3.606138094772839, -2.8520505801189175, 3.5164377843326715,
    -4.764178963044956, 5.485058062078149, -4.798747294314966,
    7.636630168389637, -20.498916206172275
];