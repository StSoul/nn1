const getRandom = (min = -0.1, max = 0.1) => Math.random() * (max - min) + min;
const sigmoid = (x) => 1 / (1 + Math.pow(Math.exp(1), -x));
const MSE = (expected, output) => (Math.pow(expected - output, 2) * 100).toFixed(2) + '%';
const addWeights = function (length) {
    let arr = [];
    for (let i = 0; i < length; i++) {
        arr.push(getRandom());
    }
    return arr;
};

function NN() {
    this.network = [];

    for (let i = 1; i < arguments.length; i++) {
        let layer = [];

        for (let a = 0; a < arguments[i]; a++) {
            layer.push({
                output: null,
                weights: addWeights(arguments[i - 1])
            });
        }
        this.network.push(layer);
    }

    this.predict = function (inputs) {
        for (let l = 0; l < this.network.length; l++) {

            for (let n = 0; n < this.network[l].length; n++) {
                let neural = this.network[l][n];
                let input = 0;

                for (let w = 0; w < neural.weights.length; w++) {
                    input += neural.weights[w] *
                        (this.network[l - 1] ?
                            this.network[l - 1][w].output :
                            inputs[w]);
                }
                neural.output = sigmoid(input);
            }
        }
        return this.network[this.network.length - 1].map(outputNeural => outputNeural.output.toFixed(10));
    };

    this.train = function (data, learningRate = 0.5, moment = 0.5) {
        const inputs = data[0], expected = data[1];
        const result = this.predict(inputs)[0];

        const error = expected - result;
        const sigmoidDerivative = result * (1 - result);
        const delta = error * sigmoidDerivative;

        for (let l = this.network.length - 1; l >= 0; l--) {
            const layer = this.network[l];
            const previousLayer = this.network[l - 1];
            const nextLayer = this.network[l + 1];

            if (l === this.network.length - 1) {
                const outputNeural = layer[0];

                for (let w = 0; w < outputNeural.weights.length; w++) {
                    outputNeural.delta = previousLayer[w].output * delta * learningRate + moment * (outputNeural.delta || 0);
                    outputNeural.weights[w] += outputNeural.delta;
                }
            } else {
                for (let n = 0; n < layer.length; n++) {
                    let neural = layer[n];
                    let errorN = 0;

                    for (let nextN = 0; nextN < nextLayer.length; nextN++) {
                        errorN += nextLayer[nextN].weights[n] * delta;
                    }

                    const derivative = neural.output * (1 - neural.output);
                    const deltaN = errorN * derivative;

                    for (let w = 0; w < neural.weights.length; w++) {
                        neural.delta = (neural.delta || 0) * moment + deltaN * learningRate * (previousLayer ?
                            previousLayer[w].output :
                            inputs[w]);
                        neural.weights[w] += neural.delta
                    }
                }
            }
        }
    };

    this.training = function (trainingSet = [], epochs = 500, learningRate = 0.5) {
        for (let i = 0; i < epochs; i++) {
            for (let a = 0; a < trainingSet.length; a++) {
                nn.train(trainingSet[a], learningRate);
            }
        }
        for (let a = 0; a < trainingSet.length; a++) {
            const expected = trainingSet[a][1];
            const output = nn.predict(trainingSet[a][0]);
            const difference = Math.abs(expected - output);
            const success = difference < 0.25;

            console.log(`Ожидалось/получено: ${trainingSet[a][1]}/${output} (${success}); Error(MSE) = ${MSE(expected, output)}`);
        }
    };
    return this;
}

const nn = new NN(400, 200, 1);

